package ru.amster.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.IProjectRepository;
import ru.amster.tm.entity.Project;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final List<Project> result = new ArrayList<>();
        for (@NotNull final Project project : records) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        for (@NotNull final Project project : records) {
            if (!userId.equals(project.getUserId())) continue;
            remove(project);
            return;
        }
    }

    @NotNull
    @Override
    public Project findOneById(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable Project result = null;
        for (@NotNull final Project project : records) {
            if (!id.equals(project.getId())) continue;
            result = project;
            break;
        }
        if (result == null) throw new EmptyProjectException();
        if (!userId.equals(result.getUserId())) throw new AccessDeniedException();
        return result;
    }

    @NotNull
    @Override
    public Project findOneByName(@Nullable final String userId, @NotNull final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable Project result = null;
        for (@NotNull final Project project : records) {
            if (!name.equals(project.getName())) continue;
            result = project;
            break;
        }
        if (result == null) throw new EmptyProjectException();
        if (!userId.equals(result.getUserId())) throw new AccessDeniedException();
        return result;
    }

    @NotNull
    @Override
    public Project findOneByIndex(@Nullable final String userId, @NotNull final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final Project project = records.get(index);
        if (project == null) throw new EmptyProjectException();
        if (!userId.equals(project.getUserId())) throw new AccessDeniedException();
        return project;
    }

    @NotNull
    @Override
    public Project removeOneByName(@Nullable final String userId, @NotNull final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final Project project = findOneByName(userId, name);
        if (project == null) throw new EmptyProjectException();
        if (!userId.equals(project.getUserId())) throw new AccessDeniedException();
        remove(project);
        return project;
    }

    @NotNull
    @Override
    public Project removeOneByIndex(@Nullable final String userId, @NotNull final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new EmptyProjectException();
        if (!userId.equals(project.getUserId())) throw new AccessDeniedException();
        remove(project);
        return project;
    }

    @NotNull
    @Override
    public Project removeOneById(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new EmptyProjectException();
        if (!userId.equals(project.getUserId())) throw new AccessDeniedException();
        remove(project);
        return project;
    }

    @NotNull
    @Override
    public Integer numberOfAllProjects(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final List<Project> result = findAll(userId);
        return result.size();
    }

}