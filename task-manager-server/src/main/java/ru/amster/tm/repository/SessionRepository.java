package ru.amster.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.ISessionRepository;
import ru.amster.tm.entity.Session;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.util.ArrayList;
import java.util.List;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Nullable
    @Override
    public List<Session> findByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final List<Session> sessions = getEntity();
        @Nullable List<Session> result = new ArrayList<>();
        for (final Session session : sessions) {
            if (userId.equals(session.getUserId())) {
                result.add(session);
            }
        }
        if (result.isEmpty()) return null;
        return result;
    }

    @Override
    public void removeByUserId(@NotNull final String userId) {
        @Nullable final List<Session> sessions = findByUserId(userId);
        if (sessions == null) return;
        for (final Session session : sessions) {
            remove(session);
        }
    }

}