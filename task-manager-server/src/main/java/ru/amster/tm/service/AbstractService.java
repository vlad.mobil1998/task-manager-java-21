package ru.amster.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.IRepository;
import ru.amster.tm.api.servise.IService;
import ru.amster.tm.entity.AbstractEntity;
import ru.amster.tm.exception.empty.EmptyEntityException;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService {

    @NotNull
    private final IRepository<E> abstractRepository;

    public AbstractService(@NotNull final IRepository<E> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final Object record) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (record == null) throw new EmptyProjectException();
        abstractRepository.remove(record);
    }

    @Nullable
    @Override
    public List<E> findAll() {
        return abstractRepository.getEntity();
    }

    @Override
    public void add(@Nullable final Object record) {
        if (record == null) throw new EmptyEntityException();
        abstractRepository.merge(record);
    }

    @Override
    public void clear() {
        abstractRepository.removeAll();
    }

}