package ru.amster.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.*;
import ru.amster.tm.dto.Domain;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Session;
import ru.amster.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService
public final class AdminUserEndpoint {

    IServiceLocator serviceLocator;

    public AdminUserEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    @NotNull
    public User lockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(session, Role.ADMIN);
        IUserService userService = serviceLocator.getUserService();
        return userService.lockUserByLogin(login);
    }

    @WebMethod
    @NotNull
    public User unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(session, Role.ADMIN);
        IUserService userService = serviceLocator.getUserService();
        return userService.unlockUserByLogin(login);
    }

    @WebMethod
    public void load(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "domain", partName = "domain") @NotNull final Domain domain
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(session, Role.ADMIN);
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
    }

    @NotNull
    @WebMethod
    public Domain export(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(session, Role.ADMIN);
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        final Domain domain = new Domain();
        domainService.export(domain);
        return domain;
    }

    @WebMethod
    @Nullable
    public List<User> findAllUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(session, Role.ADMIN);
        @NotNull final IUserService userService = serviceLocator.getUserService();
        return userService.findAll();
    }

    @WebMethod
    @NotNull
    public User findUserById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(session, Role.ADMIN);
        @NotNull final IUserService userService = serviceLocator.getUserService();
        return userService.findById(id);
    }

    @WebMethod
    @NotNull
    public User findUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(session, Role.ADMIN);
        @NotNull final IUserService userService = serviceLocator.getUserService();
        return userService.findByLogin(login);
    }

    @WebMethod
    public void removeUserById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(session, Role.ADMIN);
        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.removeById(id);
        sessionService.close(session);
    }

    @WebMethod
    public void removeUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(session, Role.ADMIN);
        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.removeByLogin(login);
        sessionService.close(session);
    }

}