package ru.amster.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.ISessionService;
import ru.amster.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public final class SessionEndpoint {

    private IServiceLocator serviceLocator;

    public SessionEndpoint(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    public Session openSession(
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        return sessionService.open(login, password);
    }

    @WebMethod
    public void closeSession(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.close(session);
    }
}