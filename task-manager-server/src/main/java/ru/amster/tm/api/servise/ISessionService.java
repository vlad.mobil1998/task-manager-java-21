package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Session;

public interface ISessionService {

    void setServiceLocator(@NotNull IServiceLocator serviceLocator);

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    Session open(@NotNull String login, @NotNull String password);

    void close(@NotNull Session session);

    void validate(@Nullable Session session);

    Session sign(@NotNull Session session);

    void validate(@NotNull Session session, @Nullable Role role);

    void signOutByLogin(@Nullable String login);

    void signOutByUserId(@Nullable String userId);
}
