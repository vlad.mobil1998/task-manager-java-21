package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    Project findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Project findOneByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project removeOneByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Project removeOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    Project updateProjectById(
            @Nullable String userId, @Nullable String id,
            @Nullable String name, @Nullable String description
    );

    @NotNull
    Project updateProjectByIndex(
            @Nullable String userId, @Nullable Integer index,
            @Nullable String name, @Nullable String description
    );

    @NotNull
    Integer numberOfAllProjects(@Nullable String userId);

    void load(@Nullable List<Project> projects);

    @NotNull
    List<Project> export();

}