package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.AbstractEntity;

import java.util.List;

public interface IService<E extends AbstractEntity> {

    @Nullable
    List<E> findAll();

    void add(@NotNull Object record);

    void remove(@Nullable String userId, @Nullable Object project);

    void clear();

}