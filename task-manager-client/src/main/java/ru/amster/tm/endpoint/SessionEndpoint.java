package ru.amster.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-02-08T20:05:39.749+03:00
 * Generated source version: 3.2.7
 */
@WebService(targetNamespace = "http://endpoint.tm.amster.ru/", name = "SessionEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface SessionEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.amster.ru/SessionEndpoint/openSessionRequest", output = "http://endpoint.tm.amster.ru/SessionEndpoint/openSessionResponse")
    @RequestWrapper(localName = "openSession", targetNamespace = "http://endpoint.tm.amster.ru/", className = "ru.amster.tm.endpoint.OpenSession")
    @ResponseWrapper(localName = "openSessionResponse", targetNamespace = "http://endpoint.tm.amster.ru/", className = "ru.amster.tm.endpoint.OpenSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.amster.tm.endpoint.Session openSession(
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.amster.ru/SessionEndpoint/closeSessionRequest", output = "http://endpoint.tm.amster.ru/SessionEndpoint/closeSessionResponse")
    @RequestWrapper(localName = "closeSession", targetNamespace = "http://endpoint.tm.amster.ru/", className = "ru.amster.tm.endpoint.CloseSession")
    @ResponseWrapper(localName = "closeSessionResponse", targetNamespace = "http://endpoint.tm.amster.ru/", className = "ru.amster.tm.endpoint.CloseSessionResponse")
    public void closeSession(
            @WebParam(name = "session", targetNamespace = "")
                    ru.amster.tm.endpoint.Session session
    );
}
