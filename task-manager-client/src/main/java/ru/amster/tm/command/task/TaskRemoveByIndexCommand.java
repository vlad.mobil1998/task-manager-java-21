package ru.amster.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.TaskEndpoint;
import ru.amster.tm.endpoint.Task;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "task-re-i";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Remove task by index";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        System.out.println("ENTER INDEX");
        @NotNull final TaskEndpoint taskEndpoint = webServiceLocator.getTaskEndpoint();
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Integer maxIndex = taskEndpoint.numberOfAllTasks(webServiceLocator.getSession());
        if (index >= maxIndex) throw new InvalidIndexException(index, maxIndex);

        @Nullable final Task task = taskEndpoint.removeTaskByIndex(webServiceLocator.getSession(), index);
        if (task == null) throw new EmptyTaskException();
        System.out.println("[OK]");
    }

}
