package ru.amster.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.ProjectEndpoint;
import ru.amster.tm.endpoint.Project;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "project-upd-id";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Update project by id";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        System.out.println("ENTER ID");
        @Nullable final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final ProjectEndpoint projectEndpoint = webServiceLocator.getProjectEndpoint();
        @Nullable final Project project = projectEndpoint.findProjectById(
                webServiceLocator.getSession(),
                id
        );
        if (project == null) throw new EmptyProjectException();

        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        System.out.println("ENTER DESCRIPTION");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final Project projectUpdate = projectEndpoint.updateProjectById(
                webServiceLocator.getSession(),
                id,
                name,
                description
        );
        if (projectUpdate == null) throw new EmptyProjectException();
        System.out.println("[OK]");
    }

}