package ru.amster.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.UserEndpoint;
import ru.amster.tm.endpoint.User;
import ru.amster.tm.exception.empty.EmptyUserException;
import ru.amster.tm.exception.user.AccessDeniedException;

public final class UserProfileShowCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "view-profile";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Show you profile";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROFILE]");
        if (webServiceLocator.getSession().getId() == null) throw new AccessDeniedException();

        @NotNull final UserEndpoint userEndpoint = webServiceLocator.getUserEndpoint();
        @Nullable final User user = userEndpoint.findUserById(
                webServiceLocator.getSession(),
                webServiceLocator.getSession().getUserId()
        );
        if (user == null) throw new EmptyUserException();

        System.out.println("[LOGIN]");
        System.out.println(user.getLogin());
        if (user.getEmail() != null && !user.getEmail().isEmpty()) {
            System.out.println("[EMAIL]");
            System.out.println(user.getEmail());
        }
        if (user.getFistName() != null && !user.getFistName().isEmpty()) {
            System.out.println("[FIST NAME]");
            System.out.println(user.getFistName());
        }
        if (user.getMiddleName() != null && !user.getMiddleName().isEmpty()) {
            System.out.println("[MIDDLE NAME]");
            System.out.println(user.getMiddleName());
        }
        if (user.getLastName() != null && !user.getLastName().isEmpty()) {
            System.out.println("[LAST NAME]");
            System.out.println(user.getLastName());
        }
        System.out.println("OK");
    }

}