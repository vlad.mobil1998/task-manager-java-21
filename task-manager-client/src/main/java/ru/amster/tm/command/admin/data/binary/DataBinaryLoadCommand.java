package ru.amster.tm.command.admin.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.admin.data.AbstractDataCommand;
import ru.amster.tm.endpoint.Domain;
import ru.amster.tm.endpoint.AdminUserEndpoint;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String name() {
        return "data-bin-load";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Load data to binary file";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA BINARY LOAD]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        @NotNull final AdminUserEndpoint adminUserEndpoint = webServiceLocator.getAdminUserEndpoint();
        adminUserEndpoint.load(webServiceLocator.getSession(), domain);
        objectInputStream.close();
        fileInputStream.close();
        System.out.println("[OK]");
    }

}