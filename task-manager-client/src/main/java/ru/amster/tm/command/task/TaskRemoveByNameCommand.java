package ru.amster.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.TaskEndpoint;
import ru.amster.tm.endpoint.Task;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public final class TaskRemoveByNameCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "task-re-name";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Remove task by name";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        System.out.println("ENTER NAME");
        @Nullable final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final TaskEndpoint taskEndpoint = webServiceLocator.getTaskEndpoint();
        @Nullable final Task task = taskEndpoint.removeTaskByName(webServiceLocator.getSession(), name);
        if (task == null) throw new EmptyTaskException();
        System.out.println("[OK]");
    }

}