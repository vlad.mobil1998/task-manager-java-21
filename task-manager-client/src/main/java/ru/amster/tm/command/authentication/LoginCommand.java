package ru.amster.tm.command.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.Session;
import ru.amster.tm.endpoint.SessionEndpoint;
import ru.amster.tm.exception.empty.EmptyLoginException;
import ru.amster.tm.exception.empty.EmptyPasswordException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public final class LoginCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "login";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Sign in system";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("[ENTER LOGIN]");
        @Nullable final String login = TerminalUtil.nextLine();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        System.out.println("[ENTER PASSWORD]");
        @Nullable final String password = TerminalUtil.nextLine();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        @NotNull final SessionEndpoint sessionEndpoint = webServiceLocator.getSessionEndpoint();
        Session sessionReceived = sessionEndpoint.openSession(login, password);
        if (sessionReceived == null) throw new AccessDeniedException();
        webServiceLocator.setSession(sessionReceived);
        System.out.println("[OK]");
    }

}